package com.evazive.common;

/**
 * @author EVAZIVE
 */
public enum ErrorNumber {

    /**
     * Client try to call '/deal' service at second time.
     */
    ERR_JACK_001("Wrong sequence of service call! You should do HIT or STAND"),

    /**
     * Client try to call '/play' service but he is does not start the game.
     */
    ERR_JACK_002("Wrong sequence of service call! You should do rate first."),

    /**
     * Player does not have enough money for game starting.
     */
    ERR_JACK_003("Not enough money for game starting! Please fill your game purse."),

    /**
     * Incorrect API calling. See specification.
     */
    ERR_JACK_004("Incorrect API calling"),

    /**
     * Player purse does not exists
     */
    ERR_JACK_005("Player purse with current id does not exists!"),

    /**
     * System error
     */
    ERR_JACK_006("System error");

    private String message;

    ErrorNumber(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
