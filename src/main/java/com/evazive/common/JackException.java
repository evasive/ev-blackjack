package com.evazive.common;

/**
 * @author EVAZIVE
 */
public class JackException extends Exception {

    private ErrorNumber errorNumber;

    public JackException(ErrorNumber errorNumber, Throwable cause) {
        super(cause);
        this.errorNumber = errorNumber;
    }

    public JackException(ErrorNumber errorNumber, String message) {
        super(message);
        this.errorNumber = errorNumber;
    }

    public JackException(ErrorNumber errorNumber) {
        this.errorNumber = errorNumber;
    }

    public ErrorNumber getErrorNumber() {
        return errorNumber;
    }
}
