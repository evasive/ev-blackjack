package com.evazive.common.handler;

import com.evazive.common.ErrorNumber;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author EVAZIVE
 */
public class ErrorResponse {

    @JsonProperty("error")
    private ErrorNumber errorNumber;
    @JsonProperty("message")
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("details")
    private String details;

    public ErrorResponse(ErrorNumber errorNumber, String message) {
        this.errorNumber = errorNumber;
        this.message = message;
    }

    public ErrorResponse(ErrorNumber errorNumber, String message, String details) {
        this.errorNumber = errorNumber;
        this.message = message;
        this.details = details;
    }

    public ErrorNumber getErrorNumber() {
        return errorNumber;
    }

    public void setErrorNumber(ErrorNumber errorNumber) {
        this.errorNumber = errorNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "errorNumber=" + errorNumber +
                ", message='" + message + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
