package com.evazive.common.handler;

import com.evazive.common.ErrorNumber;

import javax.validation.ValidationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


/**
 * @author EVAZIVE
 */
@Provider
public class ValidationExceptionHandler implements ExceptionMapper<ValidationException> {

    @Override
    public Response toResponse(ValidationException exception) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse(ErrorNumber.ERR_JACK_004,
                        ErrorNumber.ERR_JACK_004.getMessage(),
                        exception.toString()))
                .build();
    }

}
