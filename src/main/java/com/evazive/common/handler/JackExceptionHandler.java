package com.evazive.common.handler;

import com.evazive.common.ErrorNumber;
import com.evazive.common.JackException;
import com.fasterxml.jackson.databind.JsonMappingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author EVAZIVE
 */
@Provider
public class JackExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception exception) {
        if (exception instanceof JackException) {
            JackException jackException = (JackException) exception;
            ErrorNumber errorNumber = jackException.getErrorNumber();
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(new ErrorResponse(errorNumber, errorNumber.getMessage()))
                    .build();
        }
        if (exception instanceof JsonMappingException) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(new ErrorResponse(ErrorNumber.ERR_JACK_004,
                            ErrorNumber.ERR_JACK_004.getMessage()))
                    .build();
        }
        return Response
                .status(Response.Status.SERVICE_UNAVAILABLE)
                .entity(new ErrorResponse(ErrorNumber.ERR_JACK_006, ErrorNumber.ERR_JACK_006.getMessage()))
                .build();

    }
}
