package com.evazive.dao.cache.entities;

import com.evazive.core.entities.CardI;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author EVAZIVE
 */
public class Progress implements ProgressI, Serializable {

    private List<CardI> playerCards;
    private List<CardI> dealerCards;
    private Double deal;

    @Override
    public List<CardI> getPlayerCards() {
        return playerCards;
    }

    public void setPlayerCards(List<CardI> playerCards) {
        this.playerCards = playerCards;
    }

    @Override
    public List<CardI> getDealerCards() {
        return dealerCards;
    }

    public void setDealerCards(List<CardI> dealerCards) {
        this.dealerCards = dealerCards;
    }

    @Override
    public Double getDeal() {
        return deal;
    }

    public void setDeal(Double deal) {
        this.deal = deal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Progress progress = (Progress) o;
        return Objects.equals(playerCards, progress.playerCards) &&
                Objects.equals(dealerCards, progress.dealerCards) &&
                Objects.equals(deal, progress.deal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerCards, dealerCards, deal);
    }

    @Override
    public String toString() {
        return "Progress{" +
                "playerCards=" + playerCards +
                ", dealerCards=" + dealerCards +
                ", deal=" + deal +
                '}';
    }
}
