package com.evazive.dao.cache.entities;

import com.evazive.core.entities.CardI;

import java.util.List;

/**
 * @author EVAZIVE
 */
public interface ProgressI {

    /**
     * Player cards
     */
    List<CardI> getPlayerCards();

    /**
     * Dealer cards
     */
    List<CardI> getDealerCards();

    /**
     * Player deal
     */
    Double getDeal();

}
