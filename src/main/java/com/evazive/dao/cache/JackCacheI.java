package com.evazive.dao.cache;

import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.core.entities.CardI;

import javax.ejb.Local;
import java.util.List;

/**
 * @author EVAZIVE
 */
@Local
public interface JackCacheI {

    /**
     * Save game progress into cache
     *
     * @param session player identifier
     * @param value   game progress
     */
    void putProgress(String session, ProgressI value);

    /**
     * Get game progress
     *
     * @param session player identifier
     */
    ProgressI getProgress(String session);

    /**
     * Save game deck into cache
     *
     * @param session player identifier
     * @param cards   cards of deck
     */
    void putDeckCards(String session, List<CardI> cards);

    /**
     * Get cards deck for current game
     *
     * @param session player identifier
     */
    List<CardI> getDeckCards(String session);

    /**
     * Save player balance into cache
     *
     * @param session player identifier
     * @param balance player balance
     */
    void putBalance(String session, Double balance);

    /**
     * Get player purse balance
     *
     * @param session player identifier
     */
    Double getBalance(String session);

    /**
     * Clear game progress
     *
     * @param session player identifier
     */
    void clearProgress(String session);

}
