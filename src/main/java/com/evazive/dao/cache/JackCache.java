package com.evazive.dao.cache;

import com.evazive.dao.cache.entities.Progress;
import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.core.entities.CardI;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import java.util.List;

/**
 * @author EVAZIVE
 */
@Singleton
public class JackCache implements JackCacheI {

    private static final String PROGRESS = "Progress";
    private static final String DECK_CARDS = "DeckCards";
    private static final String BALANCE = "Balance";

    private CacheManager cacheManager;

    @PostConstruct
    public void init() {
        cacheManager = CacheManager.newInstance(getClass().getResource("/encache.xml"));
    }

    @Override
    public void putProgress(String session, ProgressI value) {
        getCache(session).put(new Element(PROGRESS, value));
    }

    @Override
    public ProgressI getProgress(String session) {
        Element element = getCache(session).get(PROGRESS);
        if (element == null) {
            return null;
        }
        return (Progress) element.getObjectValue();
    }

    @Override
    public void putDeckCards(String session, List<CardI> cards) {
        Cache cache = getCache(session);
        cache.put(new Element(DECK_CARDS, cards));
    }

    @Override
    public List<CardI> getDeckCards(String session) {
        Element element = getCache(session).get(DECK_CARDS);
        if (element == null) {
            return null;
        }
        return (List<CardI>) element.getObjectValue();
    }

    @Override
    public void putBalance(String session, Double balance) {
        getCache(session).put(new Element(BALANCE, balance));
    }

    @Override
    public Double getBalance(String session) {
        Element element = getCache(session).get(BALANCE);
        if (element == null) {
            return null;
        }
        return (Double) element.getObjectValue();
    }

    public void clearProgress(String session) {
        cacheManager.removeCache(session);
    }

    private Cache getCache(String session) {
        Cache cache = cacheManager.getCache(session);
        if (cache == null) {
            cacheManager.addCache(session);
        }
        return cacheManager.getCache(session);
    }

}
