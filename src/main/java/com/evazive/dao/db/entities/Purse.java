package com.evazive.dao.db.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author EVAZIVE
 */
@Entity
@Table(name = "purse")
public class Purse implements PurseI, Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private String id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "balance", nullable = false)
    private Double balance;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purse")
    private List<Operation> operations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Purse purse = (Purse) o;
        return Objects.equals(id, purse.id) &&
                Objects.equals(name, purse.name) &&
                Objects.equals(balance, purse.balance) &&
                Objects.equals(operations, purse.operations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, balance, operations);
    }

    @Override
    public String toString() {
        return "Purse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", operations=" + operations +
                '}';
    }
}
