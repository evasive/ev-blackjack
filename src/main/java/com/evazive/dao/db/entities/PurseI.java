package com.evazive.dao.db.entities;

import java.util.List;

/**
 * Table entity purse.
 *
 * @author EVAZIVE
 */
public interface PurseI {

    /**
     * @return Purse identifier
     */
    String getId();

    /**
     * @return Owner name
     */
    String getName();

    /**
     * @return Purse balance
     */
    Double getBalance();

    /**
     * @return Operations bounded with purse
     */
    List<Operation> getOperations();

}
