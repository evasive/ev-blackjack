package com.evazive.dao.db.entities;

import java.util.Date;

/**
 * Table entity that contains operations with purse.
 *
 * @author EVAZIVE
 */
public interface OperationI {

    /**
     * @return Operation time stamp
     */
    Date getTime();

    /**
     * @return type of Operation
     * @link {#OperationType}
     */
    OperationType getOperation();

    /**
     * @return Purse associated with operations
     */
    Purse getPurse();
}
