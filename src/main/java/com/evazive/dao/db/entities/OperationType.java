package com.evazive.dao.db.entities;

/**
 * @author EVAZIVE
 */
public enum OperationType {

    /**
     * Player fill his purse
     */
    REFILL,

    /**
     * Player win match
     */
    GAIN,

    /**
     * Player lose match
     */
    DROP

}
