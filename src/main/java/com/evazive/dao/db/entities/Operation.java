package com.evazive.dao.db.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.Objects;

/**
 * @author EVAZIVE
 */
@Entity
@Table(name = "operation")
public class Operation implements OperationI {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "op_id")
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "op_time", nullable = false)
    private Date time;

    @Column(name = "operation", nullable = false)
    @Enumerated(EnumType.STRING)
    private OperationType operation;

    @Column(name = "sum", nullable = false)
    private Double sum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purse_id")
    private Purse purse;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public OperationType getOperation() {
        return operation;
    }

    public void setOperation(OperationType operation) {
        this.operation = operation;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Purse getPurse() {
        return purse;
    }

    public void setPurse(Purse purse) {
        this.purse = purse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation1 = (Operation) o;
        return Objects.equals(id, operation1.id) &&
                Objects.equals(operation, operation1.operation) &&
                Objects.equals(sum, operation1.sum) &&
                Objects.equals(purse, operation1.purse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, operation, sum, purse);
    }

    @Override
    public String toString() {
        return "Operation{" +
                "id=" + id +
                ", time=" + time +
                ", operation='" + operation + '\'' +
                ", sum='" + sum + '\'' +
                ", purse=" + purse +
                '}';
    }
}
