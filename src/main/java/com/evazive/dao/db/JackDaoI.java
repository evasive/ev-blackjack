package com.evazive.dao.db;

import com.evazive.common.JackException;
import com.evazive.dao.db.entities.OperationType;

import javax.ejb.Remote;

/**
 * @author EVAZIVE
 */

@Remote
public interface JackDaoI {

    void updateBalance(String id, Double sum, OperationType operationType) throws JackException;

    Double getPurseBalance(String id);
}
