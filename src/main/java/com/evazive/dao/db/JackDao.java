package com.evazive.dao.db;

import com.evazive.common.ErrorNumber;
import com.evazive.common.JackException;
import com.evazive.dao.cache.JackCacheI;
import com.evazive.dao.db.entities.Operation;
import com.evazive.dao.db.entities.OperationType;
import com.evazive.dao.db.entities.Purse;
import com.evazive.dao.db.entities.PurseI;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

/**
 * @author EVAZIVE
 */
@Stateless
public class JackDao implements JackDaoI {

    @Inject
    private JackCacheI cache;

    @PersistenceContext(name = "Jack")
    private EntityManager em;

    @Override
    public void updateBalance(String id, Double sum, OperationType operationType) throws JackException {
        Purse purse = em.find(Purse.class, id);
        checkPurseForNotNull(purse);
        Double currBalance = calculateBalance(purse.getBalance(), sum, operationType);
        purse.setBalance(currBalance);
        cache.putBalance(id, currBalance);
        em.merge(purse);
        em.persist(buildOperation(sum, operationType, purse));
    }

    private void checkPurseForNotNull(PurseI purse) throws JackException {
        if (purse == null) {
            throw new JackException(ErrorNumber.ERR_JACK_005);
        }
    }

    @Override
    public Double getPurseBalance(String id) {
        Double balance = cache.getBalance(id);
        if (balance == null) {
            Purse purse = em.find(Purse.class, id);
            return purse.getBalance();
        }
        return balance;
    }

    private Double calculateBalance(Double balance, Double sum, OperationType operationType) {
        if (OperationType.DROP.equals(operationType)) {
            return balance - sum;
        }
        return balance + sum;
    }

    private Operation buildOperation(Double sum, OperationType operationType, Purse purse) {
        Operation operation = new Operation();
        operation.setOperation(operationType);
        operation.setSum(sum);
        operation.setPurse(purse);
        operation.setTime(new Date());
        return operation;
    }

    /**
     * for test
     */
    public void setCache(JackCacheI cache) {
        this.cache = cache;
    }

    /**
     * for test
     */
    public void setEm(EntityManager em) {
        this.em = em;
    }
}
