package com.evazive.service.controllers;

import com.evazive.common.JackException;
import com.evazive.core.processor.BlackJackProcessorI;
import com.evazive.service.ServiceConstants;
import com.evazive.service.controllers.api.request.DealRequestI;
import com.evazive.service.controllers.api.request.JackRequestI;
import com.evazive.service.controllers.api.request.PurseRequestI;
import com.evazive.service.controllers.api.response.JackResponseI;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * @author EVAZIVE on 13.07.2015
 */
@Path(ServiceConstants.ROOT)
@Stateless
public class JackService {

    @EJB
    private BlackJackProcessorI jackProcessor;

    @POST
    @Path(ServiceConstants.PURSE)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void purse(@Valid PurseRequestI request) throws JackException {
        jackProcessor.fillPurse(request);
    }

    @POST
    @Path(ServiceConstants.DEAL)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public JackResponseI deal(@Valid DealRequestI request) throws JackException {
        return jackProcessor.deal(request);
    }

    @POST
    @Path(ServiceConstants.PLAY)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public JackResponseI play(@Valid JackRequestI request) throws JackException {
        return jackProcessor.play(request);
    }

}
