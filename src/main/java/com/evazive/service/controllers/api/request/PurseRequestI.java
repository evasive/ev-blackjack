package com.evazive.service.controllers.api.request;


import com.evazive.core.Constants;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.validation.constraints.NotNull;

/**
 * @author EVAZIVE
 */
@JsonDeserialize(as = PurseRequest.class)
public interface PurseRequestI {

    @NotNull(message = Constants.ErrMessages.PLAYER_UID_NOT_NULL)
    @JsonProperty("player_id")
    String getPlayerId();

    @NotNull(message = Constants.ErrMessages.CASH_NOT_NULL)
    @JsonProperty("cash")
    Double getCash();

}
