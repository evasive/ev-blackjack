package com.evazive.service.controllers.api.response;

import com.evazive.core.entities.CardI;
import com.evazive.service.MatchStatus;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;


/**
 * @author EVAZIVE
 */
public class JackResponse implements JackResponseI, Serializable {

    private List<CardI> dealerCards;
    private List<CardI> playerCards;
    private Integer dealerScore;
    private Integer playerScore;

    private Double deal;
    private Double balance;
    private MatchStatus matchStatus;
    private Double winSum;

    @Override
    public List<CardI> getDealerCards() {
        return dealerCards;
    }

    public void setDealerCards(List<CardI> dealerCards) {
        this.dealerCards = dealerCards;
    }

    @Override
    public List<CardI> getPlayerCards() {
        return playerCards;
    }

    public void setPlayerCards(List<CardI> playerCards) {
        this.playerCards = playerCards;
    }

    @Override
    public Integer getDealerScore() {
        return dealerScore;
    }

    public void setDealerScore(Integer dealerScore) {
        this.dealerScore = dealerScore;
    }

    @Override
    public Integer getPlayerScore() {
        return playerScore;
    }

    public void setPlayerScore(Integer playerScore) {
        this.playerScore = playerScore;
    }

    @Override
    public Double getDeal() {
        return deal;
    }

    public void setDeal(Double deal) {
        this.deal = deal;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public MatchStatus getMatchStatus() {
        return matchStatus;
    }

    @Override
    public Double getWinSum() {
        return winSum;
    }

    public void setWinSum(Double winSum) {
        this.winSum = winSum;
    }

    public void setMatchStatus(MatchStatus matchStatus) {
        this.matchStatus = matchStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JackResponse that = (JackResponse) o;
        return Objects.equals(playerCards, that.playerCards) &&
                Objects.equals(dealerCards, that.dealerCards) &&
                Objects.equals(dealerScore, that.dealerScore) &&
                Objects.equals(playerScore, that.playerScore) &&
                Objects.equals(deal, that.deal) &&
                Objects.equals(balance, that.balance) &&
                Objects.equals(matchStatus, that.matchStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerCards, dealerCards, dealerScore, playerScore, deal, balance, matchStatus);
    }

    @Override
    public String toString() {
        return "JackResponse{" +
                "playerCards=" + playerCards +
                ", dealerCards=" + dealerCards +
                ", dealerScore=" + dealerScore +
                ", playerScore=" + playerScore +
                ", deal=" + deal +
                ", balance=" + balance +
                ", matchStatus=" + matchStatus +
                '}';
    }
}
