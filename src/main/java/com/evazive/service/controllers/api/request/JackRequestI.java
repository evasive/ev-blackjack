package com.evazive.service.controllers.api.request;

import com.evazive.core.Constants;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.validation.constraints.NotNull;


/**
 * @author EVAZIVE
 */
@JsonSerialize(as = JackRequest.class)
@JsonDeserialize(as = JackRequest.class)
public interface JackRequestI {

    @NotNull(message = Constants.ErrMessages.PLAYER_UID_NOT_NULL)
    @JsonProperty("player_id")
    String getPlayerId();

    @NotNull(message = Constants.ErrMessages.ACTION_NOT_NULL)
    @JsonProperty("action")
    Action getAction();

}
