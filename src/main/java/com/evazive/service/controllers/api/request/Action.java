package com.evazive.service.controllers.api.request;

/**
 * @author EVAZIVE
 */
public enum Action {
    HIT,
    STAND,
    UNKNOWN
}
