package com.evazive.service.controllers.api.response;

import com.evazive.core.entities.CardI;
import com.evazive.service.MatchStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * @author EVAZIVE
 */

@JsonSerialize(as = JackResponse.class)
@JsonDeserialize(as = JackResponse.class)
public interface JackResponseI {

    @JsonProperty("dealer_cards")
    List<CardI> getDealerCards();

    @JsonProperty("player_cards")
    List<CardI> getPlayerCards();

    @JsonProperty("dealer_score")
    Integer getDealerScore();

    @JsonProperty("player_score")
    Integer getPlayerScore();

    @JsonProperty("deal")
    Double getDeal();

    @JsonProperty("purse_balance")
    Double getBalance();

    @JsonProperty("match_status")
    MatchStatus getMatchStatus();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("win_sum")
    Double getWinSum();

}
