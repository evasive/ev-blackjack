package com.evazive.service.controllers.api.request;

import java.util.Objects;

/**
 * @author EVAZIVE
 */
public class PurseRequest implements PurseRequestI {

    private String playerId;
    private Double cash;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurseRequest that = (PurseRequest) o;
        return Objects.equals(playerId, that.playerId) &&
                Objects.equals(cash, that.cash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, cash);
    }

    @Override
    public String toString() {
        return "PurseRequest{" +
                "playerId='" + playerId + '\'' +
                ", cash=" + cash +
                '}';
    }
}
