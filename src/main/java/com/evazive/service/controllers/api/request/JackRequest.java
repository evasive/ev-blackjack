package com.evazive.service.controllers.api.request;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author EVAZIVE
 */
public class JackRequest implements JackRequestI, Serializable {

    private String playerId;
    private Action action;

    @Override
    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    @Override
    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JackRequest that = (JackRequest) o;
        return Objects.equals(playerId, that.playerId) &&
                Objects.equals(action, that.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, action);
    }

    @Override
    public String toString() {
        return "JackRequest{" +
                "playerId='" + playerId + '\'' +
                ", action=" + action +
                '}';
    }
}
