package com.evazive.service.controllers.api.request;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author EVAZIVE
 */
public class DealRequest implements DealRequestI, Serializable {

    private String playerId;
    private Double rate;

    @Override
    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    @Override
    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DealRequest that = (DealRequest) o;
        return Objects.equals(playerId, that.playerId) &&
                Objects.equals(rate, that.rate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, rate);
    }

    @Override
    public String toString() {
        return "DealRequest{" +
                "playerId='" + playerId + '\'' +
                ", rate=" + rate +
                '}';
    }
}
