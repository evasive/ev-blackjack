package com.evazive.service;


/**
 * @author EVAZIVE
 */
public enum MatchStatus {

    /**
     * Player playing match
     */
    IN_GAME,

    /**
     * Player win math
     */
    WIN,
    /**
     * Player lose match
     */
    LOSE,

    /**
     * Player and dealer score are equals to  21 points (Black Jack combo)
     */
    PUSH

}
