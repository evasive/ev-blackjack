package com.evazive.service;

/**
 * @author EVAZIVE
 */
public final class ServiceConstants {
    private ServiceConstants() {
    }

    public static final String ROOT = "/jack";
    public static final String PURSE = "/purse";
    public static final String DEAL = "/deal";
    public static final String PLAY = "/play";

}
