package com.evazive.core.entities;


import com.evazive.core.Grade;
import com.evazive.core.Suite;

import java.io.Serializable;
import java.util.Objects;

/**
 * @authorEVAZIVE
 */
public class Card implements CardI, Serializable {

    private Suite suite;
    private Grade grade;

    public Card(Suite suite, Grade grade) {
        this.suite = suite;
        this.grade = grade;
    }

    public Suite getSuite() {
        return suite;
    }

    public Grade getGrade() {
        return grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(suite, card.suite) &&
                Objects.equals(grade, card.grade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(suite, grade);
    }

    @Override
    public String toString() {
        return "Card{" +
                "suite=" + suite +
                ", grade=" + grade +
                '}';
    }
}
