package com.evazive.core.entities;

import com.evazive.core.Grade;
import com.evazive.core.Suite;

/**
 * Created by EVAZIVE
 */
public interface CardI {

    /**
     * Card suite
     */
    Suite getSuite();

    /**
     * Card grade
     */
    Grade getGrade();

}
