package com.evazive.core.entities;

import java.util.List;

/**
 * @author EVAZIVE
 */
public interface PlayerI {

    String getId();

    List<CardI> getCards();

}
