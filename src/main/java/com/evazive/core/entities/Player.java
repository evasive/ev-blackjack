package com.evazive.core.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * @author EVAZIVE
 */
public class Player implements PlayerI {

    private String id;
    private String name;
    private List<CardI> cards;
    private Boolean showCard;

    public Player() {
    }

    public Player(Boolean showCard) {
        this.showCard = showCard;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CardI> getCards() {
        if (cards == null) {
            cards = new ArrayList<>();
        }
        return cards;
    }

    public void setCards(ArrayList<CardI> cards) {
        this.cards = cards;
    }

    public Boolean isDealer() {
        return showCard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(id, player.id) &&
                Objects.equals(name, player.name) &&
                Objects.equals(cards, player.cards) &&
                Objects.equals(showCard, player.showCard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cards, showCard);
    }

    @Override
    public String toString() {
        return "Player{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", cards=" + cards +
                ", showCard=" + showCard +
                '}';
    }
}
