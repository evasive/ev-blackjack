package com.evazive.core.manager;

import com.evazive.core.entities.CardI;
import com.evazive.core.entities.PlayerI;

import javax.ejb.Local;

/**
 * @author EVAZIVE
 */
@Local
public interface DeckManagerI {

    /**
     * Create game deck with shuffle and put them into cache
     */
    void createDeck(String session);

    /**
     * Deal cards for players.
     * First game action.
     *
     * @param session player identifier
     */
    void dealCards(PlayerI player, PlayerI dealer, String session);

    /**
     * Deal card
     *
     * @param session player identifier
     * @return top card of deck
     */
    CardI dealCard(String session);

}
