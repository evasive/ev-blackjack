package com.evazive.core.manager;

import com.evazive.core.Grade;
import com.evazive.core.Suite;
import com.evazive.core.entities.Card;
import com.evazive.core.entities.CardI;
import com.evazive.core.entities.PlayerI;
import com.evazive.dao.cache.JackCacheI;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author EVAZIVE
 */
@Singleton
public class DeckManager implements DeckManagerI {

    private static final int CARDS_COUNT = 52;
    private static final int TOP = 0;

    @Inject
    private JackCacheI cache;

    public void createDeck(String session) {
        List<CardI> cards = new ArrayList<>(CARDS_COUNT);
        shuffle(cards);
        cache.putDeckCards(session, cards);
    }

    public void dealCards(PlayerI player, PlayerI dealer, String playerId) {
        player.getCards().add(dealCard(playerId));
        dealer.getCards().add(dealCard(playerId));
        player.getCards().add(dealCard(playerId));
        dealer.getCards().add(dealCard(playerId));
    }

    public CardI dealCard(String session) {
        List<CardI> cards = cache.getDeckCards(session);
        CardI card = cards.get(TOP);
        cards.remove(TOP);
        cache.putDeckCards(session, cards);
        return card;
    }

    private void shuffle(List<CardI> cards) {
        for (int s = 0; s < Suite.values().length; s++) {
            for (int g = 0; g < Grade.values().length; g++) {
                cards.add(new Card(Suite.getSuiteByIndex(s), Grade.getGradeByIndex(g)));
            }
        }
        Random generator = new Random();
        for (int i = 0; i < cards.size(); i++) {
            int randomIndex = generator.nextInt(cards.size());
            Collections.swap(cards, i, randomIndex);
        }
    }

    /**
     * for tests
     */
    public void setCache(JackCacheI cache) {
        this.cache = cache;
    }
}
