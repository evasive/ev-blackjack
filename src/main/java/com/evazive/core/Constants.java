package com.evazive.core;

/**
 * @author EVAZIVE
 */
public final class Constants {

    private Constants() {
    }

    public static final Integer MAX_SCORE = 21;
    public static final Integer BLACK_JACK = 21;
    public static final Integer DEALER_MAX = 17;
    public static final Double BLACK_JACK_RISE = 1.5;

    public static final class ErrMessages {
        private ErrMessages() {
        }

        public static final String PLAYER_UID_NOT_NULL = "'player_id' must not be null or empty!";
        public static final String CASH_NOT_NULL = "cash' must not be null or empty!";
        public static final String RATE_NOT_NULL = "'rate' must not be null or empty!";
        public static final String ACTION_NOT_NULL = "'action' must not be null or empty!";
    }

}
