package com.evazive.core.processor;

import com.evazive.core.Constants;
import com.evazive.core.converter.Converter;
import com.evazive.core.converter.ConverterI;
import com.evazive.core.manager.DeckManagerI;
import com.evazive.dao.cache.JackCacheI;
import com.evazive.common.ErrorNumber;
import com.evazive.common.JackException;
import com.evazive.core.entities.Player;
import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.dao.db.JackDaoI;
import com.evazive.dao.db.entities.OperationType;
import com.evazive.service.MatchStatus;
import com.evazive.service.controllers.api.request.Action;
import com.evazive.service.controllers.api.request.DealRequestI;
import com.evazive.service.controllers.api.request.JackRequestI;
import com.evazive.service.controllers.api.request.PurseRequestI;
import com.evazive.service.controllers.api.response.JackResponseI;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import static com.evazive.core.Constants.*;
import static com.evazive.core.processor.CardProcessor.getCardSum;
import static com.evazive.service.MatchStatus.PUSH;
import static com.evazive.service.MatchStatus.LOSE;
import static com.evazive.service.MatchStatus.WIN;
import static com.evazive.service.MatchStatus.IN_GAME;

/**
 * @author EVAZIVE
 */
@Stateless
public class BlackJackProcessor implements BlackJackProcessorI {

    @Inject
    private DeckManagerI deck;
    @Inject
    private JackCacheI cache;
    @Inject
    private ConverterI converter;
    @EJB
    private JackDaoI dao;

    @Override
    public void fillPurse(PurseRequestI request) throws JackException {
        dao.updateBalance(request.getPlayerId(), request.getCash(), OperationType.REFILL);
    }

    @Override
    public JackResponseI deal(DealRequestI request) throws JackException {
        String playerId = request.getPlayerId();
        checkIsGameIsNotInProgress(playerId);
        checkPlayerHasMoney(request);
        deck.createDeck(playerId);
        Player dealer = new Player();
        Player player = new Player();
        deck.dealCards(player, dealer, playerId);
        ProgressI progress = converter.buildProgress(player, dealer, request.getRate());
        cache.putProgress(playerId, progress);
        return calculateDealMatch(request, progress);
    }

    private void checkPlayerHasMoney(DealRequestI request) throws JackException {
        Double purseBalance = dao.getPurseBalance(request.getPlayerId());
        if (request.getRate() > purseBalance) {
            throw new JackException(ErrorNumber.ERR_JACK_003);
        }
    }

    private JackResponseI calculateDealMatch(DealRequestI request, ProgressI progress) throws JackException {
        MatchStatus matchStatus = checkForBlackJack(request, progress);
        if (WIN.equals(matchStatus) || LOSE.equals(matchStatus) || PUSH.equals(matchStatus)) {
            Double purseBalance = dao.getPurseBalance(request.getPlayerId());
            cache.clearProgress(request.getPlayerId());
            Double winSum = WIN.equals(matchStatus) ? request.getRate() * BLACK_JACK_RISE : null;
            return converter.buildBlackJackResponse(progress, purseBalance, matchStatus, winSum);
        }
        Double purseBalance = dao.getPurseBalance(request.getPlayerId());
        return converter.buildJackResponse(progress, purseBalance);
    }

    private MatchStatus checkForBlackJack(DealRequestI request, ProgressI progress) throws JackException {
        Integer dealerScore = getCardSum(progress.getDealerCards());
        Integer playerScore = getCardSum(progress.getPlayerCards());
        if (BLACK_JACK.equals(dealerScore) && BLACK_JACK.equals(playerScore)) {
            return PUSH;
        } else if (BLACK_JACK.equals(dealerScore)) {
            dao.updateBalance(request.getPlayerId(), request.getRate(), OperationType.DROP);
            return LOSE;
        } else if (BLACK_JACK.equals(playerScore)) {
            dao.updateBalance(request.getPlayerId(), request.getRate() * BLACK_JACK_RISE, OperationType.GAIN);
            return WIN;
        } else
            return IN_GAME;
    }

    @Override
    public JackResponseI play(JackRequestI request) throws JackException {
        checkGameHasProgress(request.getPlayerId());
        String playerId = request.getPlayerId();
        Action action = request.getAction();
        switch (action) {
            case HIT:
                return doHit(playerId);
            case STAND:
                return doStand(playerId);
            default:
                throw new IllegalArgumentException();
        }
    }

    private JackResponseI doHit(String playerId) throws JackException {
        ProgressI progress = cache.getProgress(playerId);
        progress.getPlayerCards().add(deck.dealCard(playerId));
        return calculateMatchAfterHit(playerId, progress);
    }

    private JackResponseI doStand(String playerId) throws JackException {
        ProgressI progress = cache.getProgress(playerId);
        doDealerHit(playerId, progress);
        return calculateMatchAfterStand(playerId, progress);
    }

    private JackResponseI calculateMatchAfterStand(String playerId, ProgressI progress) throws JackException {
        MatchStatus matchStatus = checkWinner(playerId, progress);
        Double balance = dao.getPurseBalance(playerId);
        cache.clearProgress(playerId);
        return converter.buildBlackJackResponse(progress, balance, matchStatus);
    }

    private JackResponseI calculateMatchAfterHit(String playerId, ProgressI progress) throws JackException {
        cache.putProgress(playerId, progress);
        Integer playerScore = getCardSum(progress.getPlayerCards());
        if (Constants.MAX_SCORE.equals(playerScore)) {
            doDealerHit(playerId, progress);
            MatchStatus matchStatus = checkWinner(playerId, progress);
            Double balance = dao.getPurseBalance(playerId);
            cache.clearProgress(playerId);
            return converter.buildBlackJackResponse(progress, balance, matchStatus);
        }
        if (!isBurst(playerScore)) {
            Double balance = dao.getPurseBalance(playerId);
            return converter.buildJackResponse(progress, balance);
        }
        dao.updateBalance(playerId, progress.getDeal(), OperationType.DROP);
        Double balance = dao.getPurseBalance(playerId);
        cache.clearProgress(playerId);
        return converter.buildBlackJackResponse(progress, balance, LOSE);
    }

    private MatchStatus checkWinner(String playerId, ProgressI progress) throws JackException {
        Integer playerScore = getCardSum(progress.getPlayerCards());
        Integer dealerScore = getCardSum(progress.getDealerCards());
        if (dealerScore > BLACK_JACK || playerScore > dealerScore) {
            dao.updateBalance(playerId, progress.getDeal(), OperationType.GAIN);
            return WIN;
        } else if (playerScore.equals(dealerScore)) {
            return PUSH;
        } else {
            dao.updateBalance(playerId, progress.getDeal(), OperationType.DROP);
            return LOSE;
        }
    }

    private void doDealerHit(String playerId, ProgressI progress) {
        while (getCardSum(progress.getDealerCards()) < DEALER_MAX) {
            progress.getDealerCards().add(deck.dealCard(playerId));
        }
    }

    private boolean isBurst(Integer playerScore) {
        return playerScore > Constants.MAX_SCORE;
    }

    private void checkIsGameIsNotInProgress(String userId) throws JackException {
        ProgressI progress = cache.getProgress(userId);
        if (progress != null) {
            throw new JackException(ErrorNumber.ERR_JACK_001);
        }
    }

    private void checkGameHasProgress(String userId) throws JackException {
        ProgressI progress = cache.getProgress(userId);
        if (progress == null) {
            throw new JackException(ErrorNumber.ERR_JACK_002);
        }
    }

    /**
     * for test
     */
    public void setDeck(DeckManagerI deck) {
        this.deck = deck;
    }

    /**
     * for test
     */
    public void setCache(JackCacheI cache) {
        this.cache = cache;
    }

    /**
     * for test
     */
    public void setConverter(Converter converter) {
        this.converter = converter;
    }

    /**
     * for test
     */
    public void setDao(JackDaoI dao) {
        this.dao = dao;
    }
}
