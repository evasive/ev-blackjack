package com.evazive.core.processor;

import com.evazive.common.JackException;
import com.evazive.service.controllers.api.request.DealRequestI;
import com.evazive.service.controllers.api.request.JackRequestI;
import com.evazive.service.controllers.api.request.PurseRequestI;
import com.evazive.service.controllers.api.response.JackResponseI;

import javax.ejb.Remote;

/**
 * @author EVAZIVE
 */
@Remote
public interface BlackJackProcessorI {

    /**
     * Mechanism of filling player purse
     *
     * @param request Jack API request
     * @throws JackException
     */
    void fillPurse(PurseRequestI request) throws JackException;

    /**
     * Mechanism of first cards deal
     *
     * @param request Jack API request
     * @throws JackException
     */
    JackResponseI deal(DealRequestI request) throws JackException;

    /**
     * Mechanism of calculation after HIT or STAY action
     *
     * @param request Jack API request
     * @throws JackException
     */
    JackResponseI play(JackRequestI request) throws JackException;
}
