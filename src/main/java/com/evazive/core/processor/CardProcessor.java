package com.evazive.core.processor;

import com.evazive.core.Grade;
import com.evazive.core.entities.CardI;

import java.util.List;

/**
 * Class util for calculating player card score.
 *
 * @author EVAZIVE
 */
public final class CardProcessor {
    private CardProcessor() {
    }

    private static final int BLACK_JACK = 21;

    public static Integer getCardSum(List<CardI> cards) {
        int sum = 0;
        int acesCount = 0;
        for (CardI card : cards) {
            if (Grade.ACE.equals(card.getGrade())) {
                acesCount++;
            }
            sum += card.getGrade().getGradeNumber();
        }
        return removeOverHeadIfNecessary(acesCount, sum);
    }

    private static int removeOverHeadIfNecessary(int aces, int sum) {
        while (sum > BLACK_JACK && aces > 0) {
            sum -= 10;
            aces--;
        }
        return sum;
    }
}
