package com.evazive.core;

/**
 * Enum of card suits.
 *
 * @author EVAZIVE
 */
public enum Suite {

    CLUBS,

    DIAMONDS,

    HEARTS,

    SPADES;

    public static Suite getSuiteByIndex(int index){
        for (Suite s : values()){
            if (index==s.ordinal()){
                return s;
            }
        }
        throw new IllegalArgumentException("Nonexistent index of suite!");
    }
}
