package com.evazive.core;

/**
 * @author EVAZIVE
 */
public enum Grade {

    ACE(11),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(10),
    QUEEN(10),
    KING(10);

    private Integer gradeNumber;

    Grade(Integer gradeNumber) {
        this.gradeNumber = gradeNumber;
    }

    public Integer getGradeNumber() {
        return gradeNumber;
    }

    public static Grade getGradeByIndex(int index){
        for (Grade g : values()){
            if (index==g.ordinal()){
                return g;
            }
        }
        throw new IllegalArgumentException("Nonexistent index of grade!");
    }
}
