package com.evazive.core.converter;

import com.evazive.core.processor.CardProcessor;
import com.evazive.dao.cache.entities.Progress;
import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.core.entities.CardI;
import com.evazive.core.entities.PlayerI;
import com.evazive.service.MatchStatus;
import com.evazive.service.controllers.api.response.JackResponse;
import com.evazive.service.controllers.api.response.JackResponseI;

import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.List;

/**
 * @author EVAZIVE
 */
@Singleton
public class Converter implements ConverterI {

    private static final int SECOND_CARD_INDEX = 1;
    private static final int CARDS_COUNT = 1;

    public ProgressI buildProgress(PlayerI player, PlayerI dealer, Double rate) {
        Progress progress = new Progress();
        progress.setPlayerCards(player.getCards());
        progress.setDealerCards(dealer.getCards());
        progress.setDeal(rate);
        return progress;
    }

    public JackResponseI buildBlackJackResponse(ProgressI progress, Double balance, MatchStatus status) {
        Double winSum = MatchStatus.WIN.equals(status) ? progress.getDeal() : null;
        return buildBlackJackResponse(progress, balance, status, winSum);
    }

    public JackResponseI buildBlackJackResponse(ProgressI progress, Double balance, MatchStatus status, Double winSum) {
        Integer playerScore = CardProcessor.getCardSum(progress.getPlayerCards());
        Integer dealerScore = CardProcessor.getCardSum(progress.getDealerCards());
        JackResponse jackResponse = new JackResponse();
        jackResponse.setPlayerCards(progress.getPlayerCards());
        jackResponse.setDealerCards(progress.getDealerCards());
        jackResponse.setPlayerScore(playerScore);
        jackResponse.setDealerScore(dealerScore);
        jackResponse.setDeal(progress.getDeal());
        jackResponse.setMatchStatus(status);
        jackResponse.setBalance(balance);
        jackResponse.setWinSum(winSum);
        return jackResponse;
    }

    public JackResponseI buildJackResponse(ProgressI progress, Double balance) {
        List<CardI> dealerCards = hideSecondCard(progress.getDealerCards());
        JackResponse jackResponse = new JackResponse();
        jackResponse.setPlayerCards(progress.getPlayerCards());
        jackResponse.setDealerCards(dealerCards);
        jackResponse.setPlayerScore(CardProcessor.getCardSum(progress.getPlayerCards()));
        jackResponse.setDealerScore(CardProcessor.getCardSum(dealerCards));
        jackResponse.setMatchStatus(MatchStatus.IN_GAME);
        jackResponse.setBalance(balance);
        jackResponse.setDeal(progress.getDeal());
        return jackResponse;
    }

    private List<CardI> hideSecondCard(List<CardI> dealerCards) {
        List<CardI> cards = new ArrayList<>(dealerCards);
        if (!isSecondCardHided(dealerCards.size())) {
            cards.remove(SECOND_CARD_INDEX);
        }
        return cards;
    }

    private boolean isSecondCardHided(Integer cardNumber) {
        return CARDS_COUNT == cardNumber;
    }
}
