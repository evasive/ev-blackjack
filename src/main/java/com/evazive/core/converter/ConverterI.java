package com.evazive.core.converter;

import com.evazive.core.entities.PlayerI;
import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.service.MatchStatus;
import com.evazive.service.controllers.api.response.JackResponseI;

import javax.ejb.Local;

/**
 * @author EVAZIVE
 */
@Local
public interface ConverterI {

    /**
     * Convert data adapted to cache data
     *
     * @param player Player entity
     * @param dealer Dealer entity
     * @param rate   player rate
     * @return data for caching
     */
    ProgressI buildProgress(PlayerI player, PlayerI dealer, Double rate);

    /**
     * Build finish finish game response
     *
     * @param progress math progress
     * @param balance  Player balance
     * @param status   Match status
     * @return API response
     */
    JackResponseI buildBlackJackResponse(ProgressI progress, Double balance, MatchStatus status);

    /**
     * Build finish game response
     *
     * @param progress Match progress
     * @param balance  Player balance
     * @param status   Match status
     * @param winSum   win sum
     * @return API response
     */
    JackResponseI buildBlackJackResponse(ProgressI progress, Double balance, MatchStatus status, Double winSum);

    /**
     * Build continue game response
     *
     * @param progress Match progress
     * @param balance  Player balance
     * @return API response
     */
    JackResponseI buildJackResponse(ProgressI progress, Double balance);
}
