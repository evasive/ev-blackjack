package com.evazive.dao.db;

import com.evazive.common.ErrorNumber;
import com.evazive.common.JackException;
import com.evazive.dao.cache.JackCache;
import com.evazive.dao.cache.JackCacheI;
import com.evazive.dao.db.entities.OperationType;
import com.evazive.dao.db.entities.Purse;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

/**
 * @author EVAZIVE
 */
public class JackDaoTest {

    private static final String PLAYER_UID = "testPlayerUid";

    private JackDaoI dao;
    private JackCacheI cache;
    private EntityManager em;

    @Before
    public void init() {
        JackDao dao = new JackDao();
        em = mock(EntityManager.class);
        dao.setEm(em);
        cache = mock(JackCache.class);
        dao.setCache(cache);
        this.dao = dao;
        when(em.find(any(), any())).thenReturn(JackDaoFixtures.getPurse());
    }

    @Test
    public void testUpdateBalanceWhenOperationTypeDrop() throws JackException {
        dao.updateBalance(PLAYER_UID, 100D, OperationType.DROP);
        verify(em).find(eq(Purse.class), eq(PLAYER_UID));
        verify(cache).putBalance(eq(PLAYER_UID), eq(0.0D));
        verify(em).persist(JackDaoFixtures.getDropOperation());
    }

    @Test
    public void testUpdateBalanceWhenOperationTypeGain() throws JackException {
        dao.updateBalance(PLAYER_UID, 100D, OperationType.GAIN);
        verify(em).find(eq(Purse.class), eq(PLAYER_UID));
        verify(cache).putBalance(eq(PLAYER_UID), eq(200D));
        verify(em).persist(JackDaoFixtures.getGainOperation());
    }

    @Test
    public void testUpdateBalanceWhenOperationTypeRefill() throws JackException {
        dao.updateBalance(PLAYER_UID, 100D, OperationType.REFILL);
        verify(em).find(eq(Purse.class), eq(PLAYER_UID));
        verify(cache).putBalance(eq(PLAYER_UID), eq(200D));
        verify(em).persist(JackDaoFixtures.getFillOperation());
    }

    @Test
    public void testGetPurseBalanceIfCached() {
        when(cache.getBalance(PLAYER_UID)).thenReturn(100D);
        dao.getPurseBalance(PLAYER_UID);
        verify(em, never()).find(eq(Purse.class), eq(PLAYER_UID));
    }

    @Test
    public void testGetPurseBalanceIfDoesNotCached() {
        when(cache.getBalance(PLAYER_UID)).thenReturn(null);
        dao.getPurseBalance(PLAYER_UID);
        verify(em).find(eq(Purse.class), eq(PLAYER_UID));
    }

    @Test
    public void testUserDoesNotExist() {
        when(em.find(any(), any())).thenReturn(null);
        try {
            dao.updateBalance(PLAYER_UID, 100D, OperationType.REFILL);
            fail();
        } catch (JackException e) {
            assertEquals(ErrorNumber.ERR_JACK_005, e.getErrorNumber());
        }
    }

}