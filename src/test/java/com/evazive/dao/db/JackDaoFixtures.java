package com.evazive.dao.db;

import com.evazive.dao.db.entities.Operation;
import com.evazive.dao.db.entities.OperationI;
import com.evazive.dao.db.entities.OperationType;
import com.evazive.dao.db.entities.Purse;
import com.evazive.dao.db.entities.PurseI;

import java.util.ArrayList;

/**
 * @author EVAZIVE
 */
final class JackDaoFixtures {

    static PurseI getPurse() {
        Purse purse = new Purse();
        purse.setId("testPlayerUid");
        purse.setBalance(100D);
        purse.setName("EVASIVE");
        purse.setOperations(new ArrayList<>());
        return purse;
    }

    static PurseI getPurse(Double balance) {
        Purse purse = new Purse();
        purse.setId("testPlayerUid");
        purse.setBalance(balance);
        purse.setName("EVASIVE");
        purse.setOperations(new ArrayList<>());
        return purse;
    }

    static OperationI getDropOperation() {
        Operation operation = new Operation();
        operation.setOperation(OperationType.DROP);
        operation.setPurse((Purse) getPurse(0D));
        operation.setSum(100D);
        return operation;
    }

    static OperationI getFillOperation() {
        Operation operation = new Operation();
        operation.setOperation(OperationType.REFILL);
        operation.setPurse((Purse) getPurse(200D));
        operation.setSum(100D);
        return operation;
    }

    static OperationI getGainOperation() {
        Operation operation = new Operation();
        operation.setOperation(OperationType.GAIN);
        operation.setPurse((Purse) getPurse(200D));
        operation.setSum(100D);
        return operation;
    }

}
