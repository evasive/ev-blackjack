package com.evazive.core.manager;

import com.evazive.core.entities.CardI;
import com.evazive.core.entities.Player;
import com.evazive.core.entities.PlayerI;
import com.evazive.dao.cache.JackCacheI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author EVAZIVE
 */
@RunWith(MockitoJUnitRunner.class)
public class DeckManagerTest {

    private static final String PLAYER_UID = "testPlayerUid";

    private JackCacheI cache;
    private DeckManager deckManager;

    @Captor
    private ArgumentCaptor<List<CardI>> cardsCaptor;

    @Before
    public void init() {
        deckManager = new DeckManager();
        cache = Mockito.mock(JackCacheI.class);
        deckManager.setCache(cache);
    }

    @Test
    public void testThatDealCardMethodDealHeadCardOfDeck() {
        List<CardI> deckCards = DeckManagerFixtures.getDeckBeforeDeal();
        Mockito.when(cache.getDeckCards(Mockito.anyString())).thenReturn(deckCards);
        List<CardI> expectedCards = DeckManagerFixtures.getDeckAfterDeal();
        deckManager.dealCard(PLAYER_UID);
        Mockito.verify(cache).putDeckCards(Mockito.eq(PLAYER_UID), Mockito.eq(expectedCards));
    }

    @Test
    public void testFirstDealCards() {
        List<CardI> deckCards = DeckManagerFixtures.getCardManagerTestDeck();
        Mockito.when(cache.getDeckCards(Mockito.anyString())).thenReturn(deckCards);
        PlayerI player = new Player();
        PlayerI dealer = new Player();
        player = Mockito.spy(player);
        dealer = Mockito.spy(dealer);

        deckManager.dealCards(player, dealer, PLAYER_UID);
        InOrder orderCalls = Mockito.inOrder(player, dealer, player, dealer);
        orderCalls.verify(player).getCards();
        orderCalls.verify(dealer).getCards();
        orderCalls.verify(player).getCards();
        orderCalls.verify(dealer).getCards();

        assertEquals(DeckManagerFixtures.getExpectedPlayerCards(), player.getCards());
        assertEquals(DeckManagerFixtures.getExpectedDealerCards(), dealer.getCards());
    }

    @Test
    public void testThatDeckHas52Cards() {
        deckManager.createDeck(PLAYER_UID);
        Mockito.verify(cache).putDeckCards(Mockito.eq(PLAYER_UID), cardsCaptor.capture());
        assertTrue(cardsCaptor.getValue().size() == 52);
    }

    @Test
    public void testThatDeckDoesNotHaveRepeatedCards() {
        deckManager.createDeck(PLAYER_UID);
        Mockito.verify(cache).putDeckCards(Mockito.eq(PLAYER_UID), cardsCaptor.capture());
        List<CardI> cards = cardsCaptor.getValue();
        Set<CardI> cardsFilter = new HashSet<>(cards);
        assertTrue(cardsFilter.size() == cards.size());
    }

}