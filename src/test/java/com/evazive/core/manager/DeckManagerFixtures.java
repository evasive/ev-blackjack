package com.evazive.core.manager;

import com.evazive.core.Grade;
import com.evazive.core.Suite;
import com.evazive.core.entities.Card;
import com.evazive.core.entities.CardI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author EVAZIVE
 */
final class DeckManagerFixtures {
    private DeckManagerFixtures() {
    }

    static List<CardI> getCardManagerTestDeck() {
        List<CardI> cards = new ArrayList<>();
        cards.add(new Card(Suite.HEARTS, Grade.FIVE));
        cards.add(new Card(Suite.DIAMONDS, Grade.ACE));
        cards.add(new Card(Suite.CLUBS, Grade.EIGHT));
        cards.add(new Card(Suite.SPADES, Grade.JACK));
        cards.add(new Card(Suite.HEARTS, Grade.QUEEN));
        cards.add(new Card(Suite.DIAMONDS, Grade.SEVEN));
        return cards;
    }

    static List<CardI> getExpectedPlayerCards() {
        List<CardI> cards = new ArrayList<>();
        cards.add(new Card(Suite.HEARTS, Grade.FIVE));
        cards.add(new Card(Suite.CLUBS, Grade.EIGHT));
        return cards;
    }

    static List<CardI> getExpectedDealerCards() {
        List<CardI> cards = new ArrayList<>();
        cards.add(new Card(Suite.DIAMONDS, Grade.ACE));
        cards.add(new Card(Suite.SPADES, Grade.JACK));
        return cards;
    }

    static List<CardI> getDeckBeforeDeal() {
        List<CardI> cards = new ArrayList<>();
        cards.add(new Card(Suite.HEARTS, Grade.FIVE));
        cards.add(new Card(Suite.DIAMONDS, Grade.ACE));
        return cards;
    }

    static List<CardI> getDeckAfterDeal() {
        List<CardI> cards = new ArrayList<>();
        cards.add(new Card(Suite.DIAMONDS, Grade.ACE));
        return cards;
    }

}
