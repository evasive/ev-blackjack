package com.evazive.core.processor.play;

import com.evazive.core.Grade;
import com.evazive.core.Suite;
import com.evazive.core.entities.Card;
import com.evazive.core.entities.CardI;
import com.evazive.dao.cache.entities.Progress;
import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.service.controllers.api.request.Action;
import com.evazive.service.controllers.api.request.JackRequest;
import com.evazive.service.controllers.api.request.JackRequestI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author EVAZIVE
 */
final class BlackJackProcessorPlayFixtures {

    static final String PLAYER_UID = "testPlayerUid";

    static ProgressI getPlayerProgress() {
        Progress progress = new Progress();

        List<CardI> playerCards = new ArrayList<>();
        playerCards.add(new Card(Suite.HEARTS, Grade.EIGHT));
        playerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));
        progress.setPlayerCards(playerCards);

        List<CardI> dealerCards = new ArrayList<>();
        dealerCards.add(new Card(Suite.HEARTS, Grade.KING));
        dealerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));

        progress.setDealerCards(dealerCards);
        progress.setDeal(100D);
        return progress;
    }

    static List<CardI> getGameDeck() {
        List<CardI> playerCards = new ArrayList<>();
        playerCards.add(new Card(Suite.HEARTS, Grade.KING));
        playerCards.add(new Card(Suite.HEARTS, Grade.FIVE));
        return playerCards;
    }


    static final class HitExecution {

        static JackRequestI getJacRequestWithoutAction() {
            JackRequest request = new JackRequest();
            request.setPlayerId(PLAYER_UID);
            request.setAction(Action.UNKNOWN);
            return request;
        }

        static JackRequestI getJackHitRequest() {
            JackRequest request = new JackRequest();
            request.setPlayerId(PLAYER_UID);
            request.setAction(Action.HIT);
            return request;
        }

        static ProgressI getBurstPlayerProgress() {
            Progress progress = new Progress();

            List<CardI> playerCards = new ArrayList<>();
            playerCards.add(new Card(Suite.HEARTS, Grade.TEN));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.EIGHT));
            progress.setPlayerCards(playerCards);

            List<CardI> dealerCards = new ArrayList<>();
            dealerCards.add(new Card(Suite.HEARTS, Grade.KING));
            dealerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));

            progress.setDealerCards(dealerCards);
            progress.setDeal(100D);
            return progress;
        }

        static ProgressI getUnderComboPlayerProgress() {
            Progress progress = new Progress();

            List<CardI> playerCards = new ArrayList<>();
            playerCards.add(new Card(Suite.HEARTS, Grade.NINE));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            progress.setPlayerCards(playerCards);

            List<CardI> dealerCards = new ArrayList<>();
            dealerCards.add(new Card(Suite.HEARTS, Grade.KING));
            dealerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));

            progress.setDealerCards(dealerCards);
            progress.setDeal(100D);
            return progress;
        }

        static ProgressI getUnderPushProgress() {
            Progress progress = new Progress();

            List<CardI> playerCards = new ArrayList<>();
            playerCards.add(new Card(Suite.HEARTS, Grade.NINE));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            progress.setPlayerCards(playerCards);

            List<CardI> dealerCards = new ArrayList<>();
            dealerCards.add(new Card(Suite.HEARTS, Grade.KING));
            dealerCards.add(new Card(Suite.DIAMONDS, Grade.SIX));

            progress.setDealerCards(dealerCards);
            progress.setDeal(100D);
            return progress;
        }
    }

    static final class StandExecution {
        static JackRequestI getJackStandRequest() {
            JackRequest request = new JackRequest();
            request.setPlayerId(PLAYER_UID);
            request.setAction(Action.STAND);
            return request;
        }

        static ProgressI getComboDealerProgress() {
            Progress progress = new Progress();

            List<CardI> playerCards = new ArrayList<>();
            playerCards.add(new Card(Suite.HEARTS, Grade.ACE));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.FIVE));
            progress.setPlayerCards(playerCards);

            List<CardI> dealerCards = new ArrayList<>();
            dealerCards.add(new Card(Suite.HEARTS, Grade.TWO));
            dealerCards.add(new Card(Suite.DIAMONDS, Grade.EIGHT));
            dealerCards.add(new Card(Suite.DIAMONDS, Grade.ACE));

            progress.setDealerCards(dealerCards);
            progress.setDeal(100D);
            return progress;
        }

        static ProgressI getPushProgress() {
            Progress progress = new Progress();

            List<CardI> playerCards = new ArrayList<>();
            playerCards.add(new Card(Suite.HEARTS, Grade.ACE));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.EIGHT));
            progress.setPlayerCards(playerCards);

            List<CardI> dealerCards = new ArrayList<>();
            dealerCards.add(new Card(Suite.HEARTS, Grade.EIGHT));
            dealerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            dealerCards.add(new Card(Suite.SPADES, Grade.ACE));

            progress.setDealerCards(dealerCards);
            progress.setDeal(100D);
            return progress;
        }

        static ProgressI getComboPlayerProgress() {
            Progress progress = new Progress();

            List<CardI> playerCards = new ArrayList<>();
            playerCards.add(new Card(Suite.HEARTS, Grade.ACE));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            playerCards.add(new Card(Suite.DIAMONDS, Grade.EIGHT));
            progress.setPlayerCards(playerCards);

            List<CardI> dealerCards = new ArrayList<>();
            dealerCards.add(new Card(Suite.HEARTS, Grade.KING));
            dealerCards.add(new Card(Suite.DIAMONDS, Grade.TWO));

            progress.setDealerCards(dealerCards);
            progress.setDeal(100D);
            return progress;
        }
    }
}
