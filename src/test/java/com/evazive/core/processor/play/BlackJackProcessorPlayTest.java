package com.evazive.core.processor.play;

import com.evazive.common.ErrorNumber;
import com.evazive.common.JackException;
import com.evazive.core.manager.DeckManagerI;
import com.evazive.core.processor.BlackJackFixtures;
import com.evazive.core.processor.BlackJackProcessorI;
import com.evazive.dao.cache.JackCacheI;
import com.evazive.dao.cache.entities.Progress;
import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.dao.db.JackDaoI;
import com.evazive.dao.db.entities.OperationType;
import com.evazive.service.MatchStatus;
import com.evazive.service.controllers.api.request.JackRequestI;
import com.evazive.service.controllers.api.response.JackResponseI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.evazive.core.processor.play.BlackJackProcessorPlayFixtures.*;
import static com.evazive.core.processor.play.BlackJackProcessorPlayFixtures.HitExecution.getJacRequestWithoutAction;
import static com.evazive.core.processor.play.BlackJackProcessorPlayFixtures.PLAYER_UID;
import static com.evazive.core.processor.play.BlackJackProcessorPlayFixtures.HitExecution.getJackHitRequest;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * @author EVAZIVE
 */
public class BlackJackProcessorPlayTest {

    private BlackJackProcessorI jackProcessor;

    private DeckManagerI deck;
    private JackCacheI cache;
    private JackDaoI dao;

    @Before
    public void init() {
        dao = BlackJackFixtures.createJackDao();
        cache = BlackJackFixtures.createJackCache();
        deck = BlackJackFixtures.createDeckManager(cache);
        jackProcessor = BlackJackFixtures.createJackProcessor(cache, dao, deck);
        cache.putDeckCards(PLAYER_UID, getGameDeck());
    }

    @After
    public void destroy() {
        cache.clearProgress(PLAYER_UID);
    }


    /**
     * HIT ACTION
     */
    @Test
    public void testThatGameHasProgress() {
        cache.clearProgress(PLAYER_UID);
        JackRequestI jackHitRequest = getJackHitRequest();
        try {
            jackProcessor.play(jackHitRequest);
        } catch (JackException e) {
            assertEquals(ErrorNumber.ERR_JACK_002, e.getErrorNumber());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testClientSendUnsupportedOperation() throws JackException {
        cache.putProgress(PLAYER_UID, new Progress());
        JackRequestI jackHitRequest = getJacRequestWithoutAction();
        jackProcessor.play(jackHitRequest);
    }

    @Test
    public void testWhenPlayerHitDeckDealHimCard() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, getPlayerProgress());
        jackProcessor.play(jackHitRequest);
        verify(deck).dealCard(eq(PLAYER_UID));
    }

    @Test
    public void testWhenPlayerHitAndDoesNotBurstHeStayInGame() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, getPlayerProgress());
        JackResponseI jackResponse = jackProcessor.play(jackHitRequest);

        final Integer expectedDealerScore = 10;
        final Integer expectedPlayerScore = 20;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.IN_GAME, jackResponse.getMatchStatus());
    }

    @Test
    public void testWhenPlayerHitAndBurstHeLose() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, HitExecution.getBurstPlayerProgress());
        JackResponseI jackResponse = jackProcessor.play(jackHitRequest);

        final Integer expectedDealerScore = 12;
        final Integer expectedPlayerScore = 28;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.LOSE, jackResponse.getMatchStatus());
    }

    @Test
    public void testMustClearProgressAndUpdateBalanceOnLose() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, HitExecution.getBurstPlayerProgress());
        jackProcessor.play(jackHitRequest);

        verify(cache).clearProgress(eq(PLAYER_UID));

        final Double expectedDeal = HitExecution.getUnderComboPlayerProgress().getDeal();
        verify(dao, atLeastOnce())
                .updateBalance(eq(PLAYER_UID), eq(expectedDeal), eq(OperationType.DROP));
    }


    @Test
    public void testWhenPlayerHitAndGetTwentyOnePointsThenDealerGetCardsAndEqualsScore() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, HitExecution.getUnderComboPlayerProgress());
        JackResponseI jackResponse = jackProcessor.play(jackHitRequest);

        final Integer expectedDealerScore = 17;
        final Integer expectedPlayerScore = 21;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.WIN, jackResponse.getMatchStatus());
    }

    @Test
    public void testMustClearProgressAndUpdateBalanceOnWin() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, HitExecution.getUnderComboPlayerProgress());
        jackProcessor.play(jackHitRequest);

        verify(cache).clearProgress(eq(PLAYER_UID));

        final Double expectedDeal = HitExecution.getUnderComboPlayerProgress().getDeal();
        verify(dao, atLeastOnce())
                .updateBalance(eq(PLAYER_UID), eq(expectedDeal), eq(OperationType.GAIN));

    }

    @Test
    public void testPushAfterHit() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, HitExecution.getUnderPushProgress());
        JackResponseI jackResponse = jackProcessor.play(jackHitRequest);

        final Integer expectedDealerScore = 21;
        final Integer expectedPlayerScore = 21;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.PUSH, jackResponse.getMatchStatus());
    }

    @Test
    public void testMustClearProgressOnPush() throws JackException {
        JackRequestI jackHitRequest = getJackHitRequest();
        cache.putProgress(PLAYER_UID, HitExecution.getUnderPushProgress());
        jackProcessor.play(jackHitRequest);

        verify(cache).clearProgress(eq(PLAYER_UID));

        verify(dao, never())
                .updateBalance(anyString(), anyDouble(), any(OperationType.class));
    }

    /**
     * STAND ACTION
     */
    @Test
    public void testMustGetProgressOnStandAndDealCardForDealer() throws JackException {
        JackRequestI jackHitRequest = StandExecution.getJackStandRequest();
        cache.putProgress(PLAYER_UID, getPlayerProgress());
        jackProcessor.play(jackHitRequest);
        verify(cache, times(2)).getProgress(eq(PLAYER_UID));
        verify(deck, times(1)).dealCard(eq(PLAYER_UID));
    }

    @Test
    public void testWinAfterStandIfPlayerScoreBiggerThanDealer() throws JackException {
        JackRequestI jackHitRequest = StandExecution.getJackStandRequest();
        cache.putProgress(PLAYER_UID, StandExecution.getComboPlayerProgress());
        JackResponseI jackResponse = jackProcessor.play(jackHitRequest);

        final Integer expectedDealerScore = 22;
        final Integer expectedPlayerScore = 21;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.WIN, jackResponse.getMatchStatus());
    }

    @Test
    public void testStandMustClearProgressOnWinAndUpdateBalance() throws JackException {
        JackRequestI jackHitRequest = StandExecution.getJackStandRequest();
        cache.putProgress(PLAYER_UID, StandExecution.getComboPlayerProgress());
        jackProcessor.play(jackHitRequest);
        ProgressI progress = StandExecution.getComboPlayerProgress();

        verify(cache).clearProgress(eq(PLAYER_UID));

        verify(dao, atLeastOnce())
                .updateBalance(eq(PLAYER_UID), eq(progress.getDeal()), eq(OperationType.GAIN));
    }

    @Test
    public void testPushIfPlayerAndDealerScoreAreEquals() throws JackException {
        JackRequestI jackHitRequest = StandExecution.getJackStandRequest();
        cache.putProgress(PLAYER_UID, StandExecution.getPushProgress());
        JackResponseI jackResponse = jackProcessor.play(jackHitRequest);

        final Integer expectedDealerScore = 21;
        final Integer expectedPlayerScore = 21;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.PUSH, jackResponse.getMatchStatus());
    }

    @Test
    public void testStandMustClearProgressAndNoUpdateBalanceOnPush() throws JackException {
        JackRequestI jackHitRequest = StandExecution.getJackStandRequest();
        cache.putProgress(PLAYER_UID, StandExecution.getPushProgress());
        jackProcessor.play(jackHitRequest);

        verify(cache).clearProgress(eq(PLAYER_UID));

        verify(dao, never())
                .updateBalance(anyString(), anyDouble(), any(OperationType.class));
    }

    @Test
    public void testLoseIfPlayerHasLowerScoreThenDealer() throws JackException {
        JackRequestI jackHitRequest = StandExecution.getJackStandRequest();
        cache.putProgress(PLAYER_UID, StandExecution.getComboDealerProgress());
        JackResponseI jackResponse = jackProcessor.play(jackHitRequest);

        final Integer expectedDealerScore = 21;
        final Integer expectedPlayerScore = 18;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.LOSE, jackResponse.getMatchStatus());
    }

    @Test
    public void testStandMustClearProgressAndUpdateBalanceOnLose() throws JackException {
        JackRequestI jackHitRequest = StandExecution.getJackStandRequest();
        cache.putProgress(PLAYER_UID, StandExecution.getComboDealerProgress());
        jackProcessor.play(jackHitRequest);
        ProgressI progress = StandExecution.getComboDealerProgress();

        verify(cache).clearProgress(eq(PLAYER_UID));

        verify(dao, atLeastOnce())
                .updateBalance(eq(PLAYER_UID), eq(progress.getDeal()), eq(OperationType.DROP));
    }

}
