package com.evazive.core.processor.card;

import com.evazive.core.entities.CardI;
import com.evazive.core.processor.CardProcessor;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author EVAZIVE
 */
public class CardProcessorTest {

    @Test
    public void testCalculatingCardSumWithoutAces(){
        final Integer expectedSum = 16;
        List<CardI> cards = CardProcessorFixtures.getCardsWithSumUpperElewenWithAce();
        Integer takenSum = CardProcessor.getCardSum(cards);
        assertEquals(expectedSum, takenSum);
    }

    @Test
    public void testCalculatingCardSumWhenCardsSumBellowTenAndHaveAces(){
        final Integer expectedSum = 20;
        List<CardI> cards = CardProcessorFixtures.getCardsWithSumBellowTenWithAce();
        Integer takenSum = CardProcessor.getCardSum(cards);
        assertEquals(expectedSum, takenSum);
    }

}