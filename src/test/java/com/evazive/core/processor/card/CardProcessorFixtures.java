package com.evazive.core.processor.card;

import com.evazive.core.Grade;
import com.evazive.core.Suite;
import com.evazive.core.entities.Card;
import com.evazive.core.entities.CardI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author EVAZIVE
 */
final class CardProcessorFixtures {
    private CardProcessorFixtures() {
    }

    static List<CardI> getCardsWithSumUpperElewenWithAce(){
        List<CardI> cards = new ArrayList<>();
        cards.add(new Card(Suite.HEARTS, Grade.KING));
        cards.add(new Card(Suite.HEARTS, Grade.FIVE));
        cards.add(new Card(Suite.HEARTS, Grade.ACE));
        return cards;
    }

    static List<CardI> getCardsWithSumBellowTenWithAce(){
        List<CardI> cards = new ArrayList<>();
        cards.add(new Card(Suite.HEARTS, Grade.NINE));
        cards.add(new Card(Suite.HEARTS, Grade.ACE));
        return cards;
    }
}
