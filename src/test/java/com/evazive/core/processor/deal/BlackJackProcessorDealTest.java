package com.evazive.core.processor.deal;

import com.evazive.common.ErrorNumber;
import com.evazive.common.JackException;
import com.evazive.core.Constants;
import com.evazive.core.entities.CardI;
import com.evazive.core.manager.DeckManagerI;
import com.evazive.core.processor.BlackJackFixtures;
import com.evazive.core.processor.BlackJackProcessorI;
import com.evazive.dao.cache.JackCacheI;
import com.evazive.dao.cache.entities.Progress;
import com.evazive.dao.cache.entities.ProgressI;
import com.evazive.dao.db.JackDaoI;
import com.evazive.dao.db.entities.OperationType;
import com.evazive.service.MatchStatus;
import com.evazive.service.controllers.api.request.DealRequestI;
import com.evazive.service.controllers.api.request.PurseRequestI;
import com.evazive.service.controllers.api.response.JackResponseI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.runners.MockitoJUnitRunner;

import static com.evazive.core.processor.deal.BlackJackProcessorDealFixtures.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author EVAZIVE
 */
@RunWith(MockitoJUnitRunner.class)
public class BlackJackProcessorDealTest {

    @Captor
    private ArgumentCaptor<ProgressI> progressCaptor;

    private BlackJackProcessorI jackProcessor;

    private DeckManagerI deck;
    private JackCacheI cache;
    private JackDaoI dao;

    @Before
    public void init() {
        dao = BlackJackFixtures.createJackDao();
        cache = BlackJackFixtures.createJackCache();
        deck = BlackJackFixtures.createDeckManager(cache);
        jackProcessor = BlackJackFixtures.createJackProcessor(cache, dao, deck);
    }

    @After
    public void destroy() {
        cache.clearProgress(PLAYER_UID);
    }

    @Test
    public void testDealCallWhenGameInProgress() {
        cache.putProgress(PLAYER_UID, new Progress());
        DealRequestI request = getDealRequest();
        try {
            jackProcessor.deal(request);
            fail();
        } catch (JackException e) {
            assertEquals(ErrorNumber.ERR_JACK_001, e.getErrorNumber());
        }
    }

    @Test
    public void testDealCallWhenPlayerDoesNotHaveEnoughMoneyOnHisGamePurse() {
        when(dao.getPurseBalance(PLAYER_UID)).thenReturn(EMPTY_PURSE);
        DealRequestI request = getDealRequest();
        try {
            jackProcessor.deal(request);
            fail();
        } catch (JackException e) {
            assertEquals(ErrorNumber.ERR_JACK_003, e.getErrorNumber());
        }
    }

    @Test
    public void testDealShouldCreateDeckOnce() throws JackException {
        DealRequestI request = getDealRequest();
        jackProcessor.deal(request);
        verify(deck).createDeck(eq(PLAYER_UID));
    }

    @Test
    public void testDealCardsWhenGetPush() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameEnd.getPushCombo());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        JackResponseI jackResponse = jackProcessor.deal(request);

        assertEquals(Constants.BLACK_JACK, jackResponse.getDealerScore());
        assertEquals(Constants.BLACK_JACK, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.PUSH, jackResponse.getMatchStatus());
    }

    @Test
    public void testThatNothingWasWroteIntoDbAfterPush() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameEnd.getPushCombo());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        jackProcessor.deal(request);

        verify(dao, never()).updateBalance(anyString(), anyDouble(), any(OperationType.class));
    }

    @Test
    public void testDealCardsWhenPlayerHasBlackJack() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameEnd.getPlayerCombo());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        JackResponseI jackResponse = jackProcessor.deal(request);

        final Integer expectedDealerScore = 12;
        assertEquals(expectedDealerScore, jackResponse.getDealerScore());
        assertEquals(Constants.BLACK_JACK, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.WIN, jackResponse.getMatchStatus());
    }

    @Test
    public void testDoingRateGainAfterPlayersBlackJack() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameEnd.getPlayerCombo());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        jackProcessor.deal(request);

        final Double expectedWinSum = 150D;
        verify(dao).updateBalance(PLAYER_UID, expectedWinSum, OperationType.GAIN);
    }

    @Test
    public void testDealCardsWhenDealerHasBlackJack() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameEnd.getDealerCombo());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        JackResponseI jackResponse = jackProcessor.deal(request);

        final Integer expectedPlayerScore = 15;
        assertEquals(Constants.BLACK_JACK, jackResponse.getDealerScore());
        assertEquals(expectedPlayerScore, jackResponse.getPlayerScore());
        assertEquals(MatchStatus.LOSE, jackResponse.getMatchStatus());
    }

    @Test
    public void testDoingDropAfterDealersBlackJack() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameEnd.getDealerCombo());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        jackProcessor.deal(request);

        final Double expectedWinSum = 100D;
        verify(dao).updateBalance(PLAYER_UID, expectedWinSum, OperationType.DROP);
    }

    @Test
    public void testDoingCachingGameProgress() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameContinue.getCards());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        jackProcessor.deal(request);

        verify(cache).putProgress(eq(PLAYER_UID), progressCaptor.capture());

        ProgressI progress = progressCaptor.getValue();
        final Double expectedRate = 100D;
        assertEquals(ExecutionWhenGameContinue.getExpectedPlayerCards(), progress.getPlayerCards());
        assertEquals(ExecutionWhenGameContinue.getExpectedDealerCards(), progress.getDealerCards());
        assertEquals(expectedRate, progress.getDeal());
    }

    @Test
    public void testThatIfGameContinueDealerMustHaveSecondCardHided() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameContinue.getCards());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        JackResponseI response = jackProcessor.deal(request);
        final int expectedNumberOfDealerCards = 1;
        assertTrue(expectedNumberOfDealerCards == response.getDealerCards().size());
    }

    @Test
    public void testThatIfGameContinueGameStatusMustBeInGame() throws JackException {
        DealRequestI request = getDealRequest();
        cache.putDeckCards(PLAYER_UID, ExecutionWhenGameContinue.getCards());
        doNothing().when(cache).putDeckCards(any(), anyListOf(CardI.class));
        JackResponseI response = jackProcessor.deal(request);
        assertEquals(MatchStatus.IN_GAME, response.getMatchStatus());
    }

    @Test
    public void testPurseFilling() throws JackException {
        PurseRequestI request = getPurseRequest();
        jackProcessor.fillPurse(request);
        final Double expectedSum = 100D;
        verify(dao).updateBalance(eq(PLAYER_UID), eq(expectedSum), eq(OperationType.REFILL));
    }
}