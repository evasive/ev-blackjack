package com.evazive.core.processor.deal;

import com.evazive.core.Grade;
import com.evazive.core.Suite;
import com.evazive.core.entities.Card;
import com.evazive.core.entities.CardI;

import com.evazive.service.controllers.api.request.DealRequest;
import com.evazive.service.controllers.api.request.DealRequestI;
import com.evazive.service.controllers.api.request.PurseRequest;
import com.evazive.service.controllers.api.request.PurseRequestI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author EVAZIVE
 */
final class BlackJackProcessorDealFixtures {
    private BlackJackProcessorDealFixtures() {
    }

    static final String PLAYER_UID = "testPlayerUid";
    static final Double EMPTY_PURSE = 0.0;

    static DealRequestI getDealRequest() {
        DealRequest request = new DealRequest();
        request.setRate(100D);
        request.setPlayerId(PLAYER_UID);
        return request;
    }

    static PurseRequestI getPurseRequest(){
        PurseRequest request = new PurseRequest();
        request.setPlayerId(PLAYER_UID);
        request.setCash(100D);
        return request;
    }

    /**
     * Deal method execution with LOSE, WIN, PUSH status
     */
    static final class ExecutionWhenGameEnd {

        static List<CardI> getPushCombo() {
            List<CardI> cards = new ArrayList<>();
            cards.add(new Card(Suite.HEARTS, Grade.KING));
            cards.add(new Card(Suite.DIAMONDS, Grade.ACE));
            cards.add(new Card(Suite.CLUBS, Grade.ACE));
            cards.add(new Card(Suite.SPADES, Grade.JACK));
            return cards;
        }

        static List<CardI> getPlayerCombo() {
            List<CardI> cards = new ArrayList<>();
            cards.add(new Card(Suite.HEARTS, Grade.KING));
            cards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            cards.add(new Card(Suite.CLUBS, Grade.ACE));
            cards.add(new Card(Suite.SPADES, Grade.JACK));
            return cards;
        }

        static List<CardI> getDealerCombo() {
            List<CardI> cards = new ArrayList<>();
            cards.add(new Card(Suite.HEARTS, Grade.FIVE));
            cards.add(new Card(Suite.DIAMONDS, Grade.ACE));
            cards.add(new Card(Suite.CLUBS, Grade.KING));
            cards.add(new Card(Suite.SPADES, Grade.JACK));
            return cards;
        }
    }

    /**
     * Deal method execution with IN_GAME status
     */
    static final class ExecutionWhenGameContinue {

        static List<CardI> getCards() {
            List<CardI> cards = new ArrayList<>();
            cards.add(new Card(Suite.HEARTS, Grade.FIVE));
            cards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            cards.add(new Card(Suite.CLUBS, Grade.KING));
            cards.add(new Card(Suite.SPADES, Grade.JACK));
            return cards;
        }

        static List<CardI> getExpectedPlayerCards() {
            List<CardI> cards = new ArrayList<>();
            cards.add(new Card(Suite.HEARTS, Grade.FIVE));
            cards.add(new Card(Suite.CLUBS, Grade.KING));
            return cards;
        }

        static List<CardI> getExpectedDealerCards() {
            List<CardI> cards = new ArrayList<>();
            cards.add(new Card(Suite.DIAMONDS, Grade.TWO));
            cards.add(new Card(Suite.SPADES, Grade.JACK));
            return cards;
        }
    }
}
