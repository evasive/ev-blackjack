package com.evazive.core.processor;

import com.evazive.core.converter.Converter;
import com.evazive.core.manager.DeckManager;
import com.evazive.core.manager.DeckManagerI;
import com.evazive.dao.cache.JackCache;
import com.evazive.dao.cache.JackCacheI;
import com.evazive.dao.db.JackDaoI;
import org.mockito.Mockito;

/**
 * Init methods for BlackJackProcessor
 *
 * @author EVAZIVE
 */
public final class BlackJackFixtures {
    private BlackJackFixtures() {
    }

    public static final Double PURSE_BALANCE = 5000D;

    public static JackCacheI createJackCache() {
        JackCache cache = new JackCache();
        cache.init();
        return Mockito.spy(cache);
    }

    public static JackDaoI createJackDao() {
        JackDaoI dao = Mockito.mock(JackDaoI.class);
        Mockito.when(dao.getPurseBalance(Mockito.anyString())).thenReturn(PURSE_BALANCE);
        return dao;
    }

    public static DeckManager createDeckManager(JackCacheI cache) {
        DeckManager deck = new DeckManager();
        deck.setCache(cache);
        return Mockito.spy(deck);
    }

    public static BlackJackProcessorI createJackProcessor(JackCacheI cache, JackDaoI dao, DeckManagerI deck) {
        Converter converter = new Converter();
        BlackJackProcessor jackProcessor = new BlackJackProcessor();
        jackProcessor.setCache(cache);
        jackProcessor.setConverter(converter);
        jackProcessor.setDao(dao);
        jackProcessor.setDeck(deck);
        return jackProcessor;
    }

}
