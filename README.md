### Sample API purse filling request###
```
#!json
{"player_id":"xhYaqJuklIkd","cash":1500}
```

### Sample API deal request###
```
#!json
{"player_id":"xhYaqJuklIkd","rate":1500}
```

### Sample API play request###
```
#!json
{"player_id":"xhYaqJuklIkd","action":"STAND"}
```

### Sample API response on deal/play call ###
```
#!json
{
    "dealer_cards": [
        {
            "suite": "CLUBS",
            "grade": "FIVE"
        },
        {
            "suite": "HEARTS",
            "grade": "KING"
        },
        {
            "suite": "DIAMONDS",
            "grade": "TEN"
        }
    ],
    "player_cards": [
        {
            "suite": "DIAMONDS",
            "grade": "KING"
        },
        {
            "suite": "HEARTS",
            "grade": "JACK"
        }
    ],
    "dealer_score": 25,
    "player_score": 20,
    "deal": 100,
    "purse_balance": 350900,
    "match_status": "WIN",
    "win_sum": 100
}
```
### Sample API error response ###
```
#!json
{
    "error": "ERR_JACK_004",
    "message": "Incorrect API calling"
}
```